#define _GNU_SOURCE 1
#include <tinyalsa/asoundlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <getopt.h>
#include <error.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>
#include <sched.h>
#include <unistd.h>
#include <evl/evl.h>
#include <evl/compiler.h>

static unsigned int period_count = 2;

static unsigned int period_size = 48;

static unsigned int rate = 48000;

static unsigned int bits = 16;

static unsigned int channels = 16;

static unsigned int icard, ocard = 1;

static unsigned int idev, odev;

static bool oob_mode;

static void usage(void)
{
	fprintf(stderr, "usage: evloop [options]\n");
        fprintf(stderr, "-C --cpu=<num>            set CPU affinity\n");
        fprintf(stderr, "-O --oob                  enable out-of-band mode\n");
        fprintf(stderr, "-c --channels=<num>       number of channels\n");
        fprintf(stderr, "-p --size=<num>           period size\n");
        fprintf(stderr, "-n --count=<num>          period count\n");
        fprintf(stderr, "-i --idev=<card>.<dev>    Alsa input device number\n");
        fprintf(stderr, "-o --odev=<card>.<dev>    Alsa output device number\n");
        fprintf(stderr, "-r --rate=<num>           sampling rate\n");
        fprintf(stderr, "-b --bits=<num>           bits per sample\n");
}

#define short_optlist "C:c:p:n:i:o:r:b:O"

static const struct option options[] = {
	{
		.name = "cpu",
		.has_arg = required_argument,
		.val = 'C'
	},
	{
		.name = "oob",
		.has_arg = no_argument,
		.val = 'O'
	},
	{
		.name = "channels",
		.has_arg = required_argument,
		.val = 'c'
	},
	{
		.name = "size",
		.has_arg = required_argument,
		.val = 'p'
	},
	{
		.name = "count",
		.has_arg = required_argument,
		.val = 'n'
	},
	{
		.name = "idev",
		.has_arg = required_argument,
		.val = 'i'
	},
	{
		.name = "odev",
		.has_arg = required_argument,
		.val = 'o'
	},
	{
		.name = "rate",
		.has_arg = required_argument,
		.val = 'r'
	},
	{
		.name = "bits",
		.has_arg = required_argument,
		.val = 'b'
	},
	{ /* Sentinel */ }
};

static void timespec_sub(struct timespec *__restrict r,
		  const struct timespec *__restrict t1,
		  const struct timespec *__restrict t2)
{
	r->tv_sec = t1->tv_sec - t2->tv_sec;
	r->tv_nsec = t1->tv_nsec - t2->tv_nsec;
	if (r->tv_nsec < 0) {
		r->tv_sec--;
		r->tv_nsec += 1000000000;
	}
}

void timespec_sum(struct timespec *__restrict r,
		  const struct timespec *__restrict t)
{
	r->tv_sec += t->tv_sec;
	r->tv_nsec += t->tv_nsec;
	if (r->tv_nsec >= 1000000000) {
		r->tv_sec++;
		r->tv_nsec -= 1000000000;
	}
}

static inline int64_t timespec_scalar(const struct timespec *__restrict t)
{
	return t->tv_sec * 1000000000LL + t->tv_nsec;
}

static uint64_t cycle_count;

static int64_t max_ns, min_ns = 1000000000LL, avg_ns;

static struct timespec cycletime_sum;

static void do_latency(struct timespec *delta, const char *ibuf, size_t size)
{
	long ns = timespec_scalar(delta);
	size_t n;

	if (ns > max_ns)
		max_ns = ns;
	if (ns < min_ns)
		min_ns = ns;

	timespec_sum(&cycletime_sum, delta);
	cycle_count++;
	avg_ns = timespec_scalar(&cycletime_sum) / cycle_count;

	if ((cycle_count % 1000) == 0) {
		evl_printf("%-7lld  |  min=%.3d  max=%.3d  avg=%.3d",
			cycle_count, min_ns / 1000, max_ns / 1000, avg_ns / 1000);
		/*
		 * The pipeline thread cannot keep up if printing
		 * while acquiring more than four channels. This
		 * should be offloaded to a printer thread.
		 */
		if (channels > 4 || bits > 16) {
			evl_printf("\n");
			return;
		}
		for (n = 0; n < size; n++) {
			if (!(n % 16))
				evl_printf("\n    ");
			evl_printf("%.2x ", ibuf[n - 1]);
		}
		evl_printf("\n");
	}
}

static void *pipeline_thread(void __maybe_unused *arg)
{
	struct timespec date_in, date_out, delta;
	int flags, ret, frames_out, frames_in;
	struct pcm_config iconfig, oconfig;
	struct pcm *ipcm, *opcm;
	enum pcm_format format;
	size_t size, n;
	void *buf;

	switch (bits) {
	case 32:
		format = PCM_FORMAT_S32_LE;
		break;
	case 24:
		format = PCM_FORMAT_S24_LE;
		break;
	case 16:
		format = PCM_FORMAT_S16_LE;
		break;
	default:
		error(1, EINVAL, "invalid bit count: %d", bits);
	}

	if (oob_mode) {
		ret = evl_attach_self("pipeline:%d", getpid());
		if (ret < 0)
			error(1, -ret, "evl_attach_self() failed");
	}

	memset(&iconfig, 0, sizeof(iconfig));
	iconfig.channels = channels;
	iconfig.rate = rate;
	iconfig.period_size = period_size;
	iconfig.period_count = period_count;
	iconfig.format = format;
	iconfig.silence_threshold = period_size * 2;
	iconfig.stop_threshold = period_size * 4;
	iconfig.start_threshold = period_size;

	evl_printf("oob=%s devices:%u.%u -> %u.%u, channels:%u, "
		"period size:%u, period count:%u, rate:%u, bits:%u\n",
		oob_mode ? "Y" : "n",
		ocard, odev, icard, idev, channels,
		period_size, period_count, rate, bits);

	flags = PCM_IN | PCM_TSTAMPED | (oob_mode ? PCM_OOB : 0);
	ipcm = pcm_open(icard, idev, flags, &iconfig);
	if (!ipcm || !pcm_is_ready(ipcm))
		error(1, EINVAL, "pcm_open() failed for input: %s",
			ipcm ? "?" : pcm_get_error(ipcm));

	memset(&oconfig, 0, sizeof(oconfig));
	oconfig.channels = channels;
	oconfig.rate = rate;
	oconfig.period_size = period_size;
	oconfig.period_count = period_count;
	oconfig.format = format;
	oconfig.silence_threshold = period_size * 2;
	oconfig.stop_threshold = period_size * 4;
	oconfig.start_threshold = period_size;

	flags = PCM_OUT | PCM_TSTAMPED | (oob_mode ? PCM_OOB : 0);
	opcm = pcm_open(ocard, odev, flags, &oconfig);
	if (!opcm || !pcm_is_ready(opcm))
		error(1, EINVAL, "pcm_open() failed for output: %s",
			opcm ? "?" : pcm_get_error(opcm));

	frames_out = pcm_get_buffer_size(opcm);
	size = pcm_frames_to_bytes(opcm, frames_out);
	buf = malloc(size);
	if (buf == NULL)
		error(1, ENOMEM, "malloc(%zu)", size);

	for (;;) {
		for (n = 0; n < size; n++)
			((char *)buf)[n] = n & 0xff;
		ret = pcm_writei(opcm, buf, frames_out);
		if (ret < 0)
			error(1, errno, "failed playing sample");
		pcm_get_timestamp(opcm, &date_out);
		memset(buf, 0, size);
		frames_in = pcm_readi(ipcm, buf, frames_out);
		if (frames_in != frames_out)
			error(1, errno, "failed capturing sample (out=%d vs in=%d)",
				frames_in, frames_out);
		pcm_get_timestamp(ipcm, &date_in);
		timespec_sub(&delta, &date_in, &date_out);
		do_latency(&delta, buf, size);
	}

	pcm_close(opcm);
	pcm_close(ipcm);

	return NULL;
}

static void parse_device(const char *s,
			unsigned int *card, unsigned int *dev)
{
	if (sscanf(s, "%u.%u", card, dev) != 2)
		error(1, EINVAL, "invalid device specification: %s\n", s);
}

int main(int argc, char *argv[])
{
	const struct sched_param param = { .sched_priority = 1 };
	int cpu = -1, ret, c, sig;
	pthread_attr_t attr;
	pthread_t pipeline;
	cpu_set_t cpuset;
	sigset_t sigset;

	opterr = 0;

	do {
		c = getopt_long(argc, argv, short_optlist, options, NULL);
		switch (c) {
		case 'C':
			cpu = atoi(optarg);
			break;
		case 'O':
			oob_mode = true;
			break;
		case 'c':
			channels = atoi(optarg);
			break;
		case 'p':
			period_size = atoi(optarg);
			break;
		case 'n':
			period_count = atoi(optarg);
			break;
		case 'i':
			parse_device(optarg, &icard, &idev);
			break;
		case 'o':
			parse_device(optarg, &ocard, &odev);
			break;
		case 'r':
			rate = atoi(optarg);
			break;
		case 'b':
			bits = atoi(optarg);
			break;
		case EOF:
			break;
		default:
			usage();
			return 2;
		}
	} while (c != EOF && c != '?');

	if (cpu != -1) {
		CPU_ZERO(&cpuset);
		CPU_SET(cpu, &cpuset);
		ret = sched_setaffinity(0, sizeof(cpuset), &cpuset);
		if (ret)
			error(1, errno, "invalid CPU number: %d", cpu);
		evl_printf("running on CPU%d\n", cpu);
	}

	sigemptyset(&sigset);
	sigaddset(&sigset, SIGINT);
	sigaddset(&sigset, SIGTERM);
	sigaddset(&sigset, SIGHUP);
	pthread_sigmask(SIG_BLOCK, &sigset, NULL);

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
	pthread_attr_setschedparam(&attr, &param);
	pthread_attr_setstacksize(&attr, EVL_STACK_DEFAULT);
	ret = pthread_create(&pipeline, &attr, pipeline_thread, NULL);
	if (ret)
		error(1, ret, "cannot spawn pipeline thread");

	sigwait(&sigset, &sig);
	pthread_cancel(pipeline);
	pthread_join(pipeline, NULL);

	return 0;
}
