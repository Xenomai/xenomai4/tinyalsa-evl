#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <error.h>
#include <errno.h>
#include <pthread.h>
#include <sched.h>
#include <tinyalsa/pcm.h>
#include <evl/evl.h>

#define CARD_NUM  0
#define DEV_NUM   0

static const struct pcm_config config_in = {
        .channels = 2,
        .rate = 48000,
        .format = PCM_FORMAT_S16_LE,
        .period_size = 1024,
        .period_count = 2,
        .start_threshold = 1024,
        .silence_threshold = 1024 * 2,
        .stop_threshold = 1024 * 2
};

static const struct pcm_config config_out = {
        .channels = 2,
        .rate = 48000,
        .format = PCM_FORMAT_S16_LE,
        .period_size = 1024,
        .period_count = 2,
        .start_threshold = 1024,
        .silence_threshold = 1024 * 2,
	/*
	 * We capture then write out quickly enough for filling the
	 * output buffer slightly before the next playback DMA event
	 * arrives, so we need room for two full playback buffers
	 * (i.e. 2 x 2 * 1024 frames).
	 */
        .stop_threshold = 1024 * 4
};

static struct pcm *open_device(int flags, const struct pcm_config *config)
{
	struct pcm *pcm;

	/* Open a capture/playback device in out-of-band mode. */

	pcm = pcm_open(CARD_NUM, DEV_NUM, flags | PCM_OOB, config);
	if (!pcm)
		error(ENOMEM, 1, "pcm_open(%d, %d)", CARD_NUM, DEV_NUM);

	if (!pcm_is_ready(pcm)) {
		fprintf(stderr, "pcm_open(%d, %d): %s\n",
			CARD_NUM, DEV_NUM, pcm_get_error(pcm));
		exit(EXIT_FAILURE);
	}

	return pcm;
}

static void transform_audio_data(void *buf, size_t bytes)
{
	/* ... */
}

int main(int argc, char *argv[])
{
	int frames_in, frames_out, fdout, fdproxy, all_frames = 0;
	struct pcm *pcm_in, *pcm_out;
	struct sched_param param;
	size_t bufsz;
	ssize_t ret;
	void *buf;

	param.sched_priority = 1;
	if (pthread_setschedparam(pthread_self(), SCHED_FIFO, &param)) {
	        fprintf(stderr, "unable to switch to SCHED_FIFO\n");
		return 1;
	}

	ret = evl_attach_self("pcm:%d", getpid());
	if (ret < 0)
		error(-ret, 1, "evl_attach_self()");

	pcm_in = open_device(PCM_IN, &config_in);
	pcm_out = open_device(PCM_OUT, &config_out);

	/*
	 * For demo purpose, plan for dumping all processed frames to
	 * a dummy disk file named "dump.raw". Since we want to keep
	 * ultra-low latency inside the work loop, offload the actual
	 * task of interacting with the filesystem to an EVL proxy
	 * element.
	 */
	fdout = open("dump.raw", O_WRONLY | O_CREAT | O_TRUNC, 0666);
	if (fdout < 0)
		error(errno, 1, "open()");

	bufsz = pcm_frames_to_bytes(pcm_out, pcm_get_buffer_size(pcm_out));

	/*
	 * To account for (massive) delays in the regular kernel,
	 * create a proxy which is able to hold a hundred times the
	 * audio buffer. Belt and suspenders, this is way much more
	 * than needed practically.
	 */
	fdproxy = evl_new_proxy(fdout, bufsz * 100, 0, "pcm:%d", getpid());
	if (fdproxy < 0)
		error(-fdproxy, 1, "evl_new_proxy()");
	/*
	 * Switch the proxy to non-blocking I/O to receive EAGAIN from
	 * oob_write(), when/if the system eventually lags insanely
	 * (i.e. we would be able to fill the proxy buffer faster than
	 * it could drain).
	 */
	fcntl(fdproxy, F_SETFL, O_NONBLOCK);

	/* This is our PCM audio buffer. */
	buf = malloc(bufsz);
	if (buf == NULL)
		error(ENOMEM, 1, "malloc(%zu)", bufsz);

	for (;;) {
		/*
		 * Read frames in from the capture device via an
		 * out-of-band I/O request.
		 */
		frames_in = pcm_readi(pcm_in, buf, pcm_get_buffer_size(pcm_in));
		if (frames_in < 0) {
			fprintf(stderr, "pcm_readi(%d, %d): %s\n",
				CARD_NUM, DEV_NUM, pcm_get_error(pcm_in));
			exit(EXIT_FAILURE);
		}

		/* Process/transform the audio samples at will. */
		transform_audio_data(buf, pcm_frames_to_bytes(pcm_in, frames_in));

		/*
		 * Write frames out to the playback device via an
		 * out-of-band I/O request.
		 */
		frames_out = pcm_writei(pcm_out, buf, frames_in);
		if (frames_out < 0) {
			fprintf(stderr, "pcm_writei(%d, %d, fr=%d): %s\n",
				CARD_NUM, DEV_NUM, frames_in, pcm_get_error(pcm_out));
			exit(EXIT_FAILURE);
		}

		/*
		 * The proxy will carry out the length and
		 * non-deterministic job of writing to the file for
		 * us, we just need to sent it the data we want to
		 * dump there, which is a simple and time-bounded
		 * out-of-band operation.
		 */
		ret = oob_write(fdproxy, buf, pcm_frames_to_bytes(pcm_out, frames_out));
		if (ret < 0)
			error(-ret, 1, "oob_write(fdproxy)");

		/*
		 * Send some feedback to user: regular printf(3) would
		 * kick us out of out-of-band mode, so use libevl's
		 * evl_printf() instead - which streams the output
		 * through an internal proxy.
		 */
		all_frames += frames_out;
		evl_printf("%d frames processed\n", all_frames);
	}

	return 0;	/* not reached */
}
